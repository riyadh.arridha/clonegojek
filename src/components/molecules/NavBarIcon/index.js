import React from 'react';
import {View, Text, Image} from 'react-native';

const NavBarIcon = (props) => {
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
      }}>
      {/* //Kalau masukkan gambar atau icon bisa d masukkan d sini untuk menggantikan kotak putih */}
      {/* Kalau dari url bisa menggunakan <Image source={{uri:'www.google.com'}} */}
      {/* untuk gambar lokal bisa pakai require ('path gambar') atau d impor dulu aja */}
      <Image style={{height: 26, width: 26}} source={props.img} />
      <Text
        style={{
          fontSize: 10,
          color: props.active ? '#43AB4A' : '#545454',
          marginTop: 4,
        }}>
        {props.title}
      </Text>
    </View>
  );
};

export default NavBarIcon;
