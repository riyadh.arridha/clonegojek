import React from 'react';
import {View, Text, Image} from 'react-native';

const GopayFeature = (props) => {
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
      }}>
      {/* pemanggilan props tidak menggunakan this. */}
      <Image source={props.img} />
      <Text
        style={{
          fontSize: 13,
          fontWeight: 'bold',
          color: 'white',
          marginTop: 15,
        }}>
        {props.title}
      </Text>
    </View>
  );
};

export default GopayFeature;
