import React from 'react';
import {View, Text, Image} from 'react-native';
const ScrollableItem = (props) => {
  return (
    <View style={{marginRight: 16}}>
      <View
        style={{
          width: 150,
          height: 150,
          borderRadius: 10,
        }}>
        <Image
          source={props.img}
          style={{
            width: undefined,
            height: undefined,
            resizeMode: 'cover',
            flex: 1,
          }}
        />
        <Text
          style={{
            marginTop: 12,
            fontSize: 16,
            color: '#1c1c1c',
            fontWeight: 'bold',
          }}>
          {props.title}
        </Text>
      </View>
    </View>
  );
};
export default ScrollableItem;
