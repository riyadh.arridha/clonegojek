import React, {Component} from 'react';
import {Text, View, Image, ScrollView} from 'react-native';
import ScrollableItem from './../../../components/molecules/ScrollableItem';

class ScrollableProducts extends Component {
  render() {
    return (
      <View>
        <View
          style={{
            height: 15,
            width: 50,
            marginLeft: 16,
          }}>
          <Image
            source={require('../../../assets/logo/go-food.png')}
            style={{
              width: undefined,
              height: undefined,
              resizeMode: 'contain',
              flex: 1,
            }}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 16,
            paddingHorizontal: 16,
          }}>
          <Text style={{fontSize: 17, color: '#1c1c1c', fontWeight: 'bold'}}>
            Nearby Restaurant
          </Text>
          <Text style={{fontSize: 17, color: '#61a756', fontWeight: 'bold'}}>
            See All
          </Text>
        </View>
        <ScrollView
          horizontal={true}
          style={{flexDirection: 'row', paddingLeft: 16}}>
          <ScrollableItem
            title="KFC AOEN Mall"
            img={require('./../../../assets/dummy/go-food-kfc.jpg')}
          />
          <ScrollableItem
            title="Bakmi GM Aeon Mall"
            img={require('../../../assets/dummy/go-food-gm.jpg')}
          />
          <ScrollableItem
            title="Martabak Orins"
            img={require('../../../assets/dummy/go-food-orins.jpg')}
          />
          <ScrollableItem
            title="Martabak Bangka"
            img={require('../../../assets/dummy/go-food-banka.jpg')}
          />
          <ScrollableItem
            title="KFC AOEN Mall"
            img={require('../../../assets/dummy/go-food-pak-boss.jpg')}
          />
        </ScrollView>
        <View
          style={{
            borderBottomColor: '#E8E9ED',
            borderBottomWidth: 1,
            marginTop: 16,
            marginHorizontal: 16,
            marginBottom: 20,
          }}
        />
      </View>
    );
  }
}

export default ScrollableProducts;
