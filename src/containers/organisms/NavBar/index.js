import React, {Component} from 'react';
import {View} from 'react-native';
import NavBarIcon from '../../../components/molecules/NavBarIcon';
class NavBar extends Component {
  render() {
    return (
      <View style={{height: 54, flexDirection: 'row'}}>
        <NavBarIcon
          title="Home"
          img={require('./../../../assets/icon/home-active.png')}
          active
        />
        <NavBarIcon
          title="Orders"
          img={require('./../../../assets/icon/order.png')}
          // active={false} //jika false bisa d kosongkan saja tergantung, tapi tergatung logic d tujuan
        />
        <NavBarIcon
          title="Help"
          img={require('./../../../assets/icon/help.png')}
          active={false}
        />
        <NavBarIcon
          title="Inbox"
          img={require('./../../../assets/icon/inbox.png')}
          active={true}
        />
        <NavBarIcon
          title="Account"
          img={require('./../../../assets/icon/account.png')}
        />
      </View>
    );
  }
}

export default NavBar;
