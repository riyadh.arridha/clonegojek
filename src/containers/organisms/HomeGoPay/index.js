import React, {Component} from 'react';
import {View} from 'react-native';
import GopayFeature from '../../../components/molecules/GopayFeature';

export class HomeGoPay extends Component {
  render() {
    return (
      <View style={{marginHorizontal: 17, marginTop: 8}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: '#2C5FB8',
            borderTopLeftRadius: 4,
            borderTopRightRadius: 4,
            padding: 14,
          }}>
          <Image source={require('./src/assets/icon/gopay.png')}></Image>
          <Text style={{color: 'white', fontWeight: 'bold', fontSize: 17}}>
            Rp. 50.000
          </Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            paddingTop: 20,
            paddingBottom: 14,
            backgroundColor: '#2f65Bd',
          }}>
          <GopayFeature
            title="Pay"
            img={require('./src/assets/icon/pay.png')}
          />
          <GopayFeature
            title="Nearby"
            img={require('./src/assets/icon/nearby.png')}
          />
          <GopayFeature
            title="Top Up"
            img={require('./src/assets/icon/topup.png')}
          />
          <GopayFeature
            title="More"
            img={require('./src/assets/icon/more.png')}
          />
        </View>
      </View>
    );
  }
}

export default HomeGoPay;
