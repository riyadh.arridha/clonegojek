import React, {Component} from 'react';
import {StyleSheet, ScrollView, View} from 'react-native';
import SearchFeature from './src/components/molecules/SearchFeature';
import GoNews from './src/components/molecules/GoNews';
import GoInfo from './src/components/molecules/GoInfo';
import GoBanner from './src/components/molecules/GoBanner';
import ScrollableProducts from './src/containers/organisms/ScrollableProducts';
import NavBar from './src/containers/organisms/NavBar';
import HomeMainFeature from './src/containers/organisms/HomeMainFeature';
import HomeGoPay from './src/containers/organisms/HomeGoPay';

// fungsional component
// d simpan dalam folder component
// krn tdk ada state (stateless)
//nama lain, arrow function
// const Button1 = () => {
//   return (
//     <View>
//       <Text>Button</Text>
//     </View>
//   );
// };

//model fungsional komponen yang kedua
//sudah jarang d gunakan apalagi d es 6
// function Button2() {
//   return (
//     <View>
//       <Text>Button</Text>
//     </View>
//   );
// }
//class component
// class Button3 extends Component {
//   //ini bisa punya state
//   state = {};
//   render() {
//     return (
//       <View>
//         <Text>Button</Text>
//       </View>
//     );
//   }
// }

//tidak perlu pakai this utk mengakses props.
//tp d masukkan sebagai parameter

//bagian ini d pindahkan ke moleculs, krn terdiri dari image dan text
// serta msh bersifat stateless
// const GopayFeature = (props) => {
//   return (
//     <View
//       style={{
//         flex: 1,
//         alignItems: 'center',
//         justifyContent: 'center',
//         borderBottomLeftRadius: 4,
//         borderBottomRightRadius: 4,
//       }}>
//       {/* pemanggilan props tidak menggunakan this. */}
//       <Image source={props.img} />
//       <Text
//         style={{
//           fontSize: 13,
//           fontWeight: 'bold',
//           color: 'white',
//           marginTop: 15,
//         }}>
//         {props.title}
//       </Text>
//     </View>
//   );
// };

//kalau class component, pada props pakai this,
//tp krn ini sifatnya stateless, jadi utk d moleculs, bkn komponen
//ini yang d masukkan
// class GoPayFeature1 extends Component {
//   render() {
//     return (
//       <View
//         style={{
//           flex: 1,
//           alignItems: 'center',
//           justifyContent: 'center',
//           borderBottomLeftRadius: 4,
//           borderBottomRightRadius: 4,
//         }}>
//         {/* pemanggilan props menggunakan this */}
//         <Image source={this.props.img} />
//         <Text
//           style={{
//             fontSize: 13,
//             fontWeight: 'bold',
//             color: 'white',
//             marginTop: 15,
//           }}>
//           {this.props.title}
//         </Text>
//       </View>
//     )
//   }
// }

export default class App extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <ScrollView style={{flex: 1, backgroundColor: 'white'}}>
          {/* search bar */}
          <SearchFeature />
          {/* gopay */}
          <HomeGoPay />
          {/* main feature */}
          <HomeMainFeature />
          {/* News Section */}
          <GoNews
            content="Dimas Drajat selamatkan penalti, Timnas U-23 kalahkan Brunei"
            img={require('./src/assets/dummy/sepak-bola.jpg')}
          />
          {/* Section internal information */}
          <GoInfo />
          {/* Go-Food Banner Section */}
          <GoBanner />
          {/* Go Food Nearby Restaurant */}
          <ScrollableProducts />
        </ScrollView>
        {/* Bottom Tab */}
        <NavBar />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
});
